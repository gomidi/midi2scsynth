package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi2scsynth"
	"gitlab.com/gomidi/rtmididrv"
)

var sigchan = make(chan os.Signal, 10)

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v", err)
	}
}

func run() error {
	m := midi2scsynth.New("theremin")
	drv, err := rtmididrv.New()
	if err != nil {
		return err
	}
	in, err := midi.OpenIn(drv, 10, "")
	if err != nil {
		return err
	}

	err = m.Listen(in)
	if err != nil {
		return err
	}

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted...")

	return m.Close()
}
