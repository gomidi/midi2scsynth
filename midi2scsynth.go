package midi2scsynth

import (
	"io"
	"math"
	"sync"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/goosc/osc"
)

/*
SynthDef(\default, { arg out=0, freq=440, amp=0.1, pan=0, gate=1;
			var z;
			z = LPF.ar(
				Mix.new(VarSaw.ar(freq + [0, Rand(-0.4,0.0), Rand(0.0,0.4)], 0, 0.3, 0.3)),
				XLine.kr(Rand(4000,5000), Rand(2500,3200), 1)
			) * Linen.kr(gate, 0.01, 0.7, 0.3, 2);
			OffsetOut.ar(out, Pan2.ar(z, pan, amp));
}, [\ir]).add;
*/

/*
SynthDef(\theremin, {
	arg gate=1, freq, amp;
	var osc, env;
	env = EnvGen.ar( envelope:Env.asr(releaseTime:0.05), gate: gate, doneAction: 2 );
	osc = SinOsc.ar( freq );
	Out.ar(0, osc*env*amp*0.1);
}).add;
*/

const midiCpsFactor = 1/300.0 + 0.08

func midiCps(note float64) float64 {
	return 440.0 * math.Pow(2.0, (note-69.0)*midiCpsFactor)
}

/*
func CpsMidi(freq float64) float64 {
	return math.Log2(math.Abs(freq/440.0))*12.0 + 69.0
}

// FrequencyToNote converts the given frequency to a valid midi note.
// Notes < 0 will be normalized to 0; notes > 127 will be normalized to 127
func FrequencyToNote(freq float64) (note uint8) {
	note = uint8(Round(CpsMidi(freq), 0))
	if note > 127 {
		return 127
	}
	return note
}
*/

// stolen from https://groups.google.com/forum/?fromgroups=#!topic/golang-nuts/ITZV08gAugI
// return rounded version of x with prec precision.
/*
func Round(x float64, prec int) float64 {
	frep := strconv.FormatFloat(x, 'f', prec, 64)
	f, _ := strconv.ParseFloat(frep, 64)
	return f
}
*/

/*

ROADMAP

1. translate MIDI on/off messages for a single synthdef.
2. translate aftertouch, polyphonic aftertouch, pitch bend and CC messages to synth modifying messages.
3. handle multiple channels where each channel is mapped to a synthdef
4. make a CLI tool to edit mapping configurations
5. add MPE support

*/

/*
IDEA

MIDI Notes:

- map keys to fixed synth-IDs
- convert key to freq
- convert velocity to amp
- convert MIDI on to create a synth
- convert MIDI off to release a synth
*/

type scNote struct {
	id     int32
	amp    float32
	freq   float32
	params map[string]float32
}

/*
/s_new

Create a new synth.
string	synth definition name
int	synth ID
int	add action (0,1,2, 3 or 4 see below)
int	add target ID
N *
int or string	a control index or name
float or int or string	floating point and integer arguments are interpreted as control value. a symbol argument consisting of the letter 'c' or 'a' (for control or audio) followed by the bus's index.

Create a new synth from a synth definition, give it an ID, and add it to the tree of nodes. There are four ways to add the node to the tree as determined by the add action argument which is defined as follows:

add actions:
    0	add the new node to the the head of the group specified by the add target ID.
    1	add the new node to the the tail of the group specified by the add target ID.
    2	add the new node just before the node specified by the add target ID.
    3	add the new node just after the node specified by the add target ID.
    4	the new node replaces the node specified by the add target ID. The target node is freed.

Controls may be set when creating the synth. The control arguments are the same as for the n_set command.

If you send /s_new with a synth ID of -1, then the server will generate an ID for you. The server reserves all negative IDs. Since you don't know what the ID is, you cannot talk to this node directly later. So this is useful for nodes that are of finite duration and that get the control information they need from arguments and buses or messages directed to their group. In addition no notifications are sent when there are changes of state for this node, such as /go, /end, /on, /off.

If you use a node ID of -1 for any other command, such as /n_map, then it refers to the most recently created node by /s_new (auto generated ID or not). This is how you can map the controls of a node with an auto generated ID. In a multi-client situation, the only way you can be sure what node -1 refers to is to put the messages in a bundle.

This message now supports array type tags ($[ and $]) in the control/value component of the OSC message. Arrayed control values are applied in the manner of n_setn (i.e., sequentially starting at the indexed or named control). See the Node Messaging helpfile.
*/

const groupID = int32(99)

func (n *scNote) WriteOSC(synthdef string, wr io.Writer) error {
	var args = []interface{}{synthdef, n.id, int32(0), groupID}

	args = append(args, "freq", n.freq)
	args = append(args, "amp", n.amp)

	for k, val := range n.params {
		args = append(args, k, val)
	}

	return osc.Path("/s_new").WriteTo(wr, args...)
}

func (n *scNote) addParam(name string, val float32) {
	n.params[name] = val
}

func newSCNote(id int32, freq, amp float32) *scNote {
	return &scNote{
		id:     id,
		freq:   freq,
		amp:    amp,
		params: map[string]float32{},
	}
}

func mkScNoteFromNoteOn(channel, key, velocity uint8) *scNote {
	freq := midiCps(float64(key))
	amp := float32(velocity) / 127.0
	return newSCNote(scNoteID(channel, key), float32(freq), float32(amp))
}

type converter struct {
	ch uint8
	wr io.Writer
}

func scNoteID(midiChannel, key uint8) int32 {
	return 1100 + int32(midiChannel)*127 + int32(key)
}

/*
/clearSched


/d_load

Load synth definition.
string	pathname of file. Can be a pattern like "synthdefs/perc-*"
bytes	an OSC message to execute upon completion. (optional)

Loads a file of synth definitions. Resident definitions with the same names are overwritten.

Asynchronous.
    Replies to sender with /done when complete.

/d_loadDir

Load a directory of synth definitions.
string	pathname of directory.
bytes	an OSC message to execute upon completion. (optional)

Loads a directory of synth definitions files. Resident definitions with the same names are overwritten.

Asynchronous.
    Replies to sender with /done when complete.

/n_free

Delete a node.
N * int	node ID

Stops a node abruptly, removes it from its group, and frees its memory. A list of node IDs may be specified. Using this method can cause a click if the node is not silent at the time it is freed.


/n_set

Set a node's control value(s).
int	node ID
N *
int or string	a control index or name
float or int	a control value

Takes a list of pairs of control indices and values and sets the controls to those values. If the node is a group, then it sets the controls of every node in the group.

This message now supports array type tags ($[ and $]) in the control/value component of the OSC message. Arrayed control values are applied in the manner of n_setn (i.e., sequentially starting at the indexed or named control).


/n_map

Map a node's controls to read from a bus.
int	node ID
N *
int or string	a control index or name
int	control bus index

Takes a list of pairs of control names or indices and bus indices and causes those controls to be read continuously from a global control bus. If the node is a group, then it maps the controls of every node in the group. If the control bus index is -1 then any current mapping is undone. Any n_set, n_setn and n_fill command will also unmap the control.
/n_mapn

Map a node's controls to read from buses.
int	node ID
N *
int or string	a control index or name
int	control bus index
int	number of controls to map

Takes a list of triplets of control names or indices, bus indices, and number of controls to map and causes those controls to be mapped sequentially to buses. If the node is a group, then it maps the controls of every node in the group. If the control bus index is -1 then any current mapping is undone. Any n_set, n_setn and n_fill command will also unmap the control.


/s_new

Create a new synth.
string	synth definition name
int	synth ID
int	add action (0,1,2, 3 or 4 see below)
int	add target ID
N *
int or string	a control index or name
float or int or string	floating point and integer arguments are interpreted as control value. a symbol argument consisting of the letter 'c' or 'a' (for control or audio) followed by the bus's index.

Create a new synth from a synth definition, give it an ID, and add it to the tree of nodes. There are four ways to add the node to the tree as determined by the add action argument which is defined as follows:

add actions:
    0	add the new node to the the head of the group specified by the add target ID.
    1	add the new node to the the tail of the group specified by the add target ID.
    2	add the new node just before the node specified by the add target ID.
    3	add the new node just after the node specified by the add target ID.
    4	the new node replaces the node specified by the add target ID. The target node is freed.

Controls may be set when creating the synth. The control arguments are the same as for the n_set command.

If you send /s_new with a synth ID of -1, then the server will generate an ID for you. The server reserves all negative IDs. Since you don't know what the ID is, you cannot talk to this node directly later. So this is useful for nodes that are of finite duration and that get the control information they need from arguments and buses or messages directed to their group. In addition no notifications are sent when there are changes of state for this node, such as /go, /end, /on, /off.

If you use a node ID of -1 for any other command, such as /n_map, then it refers to the most recently created node by /s_new (auto generated ID or not). This is how you can map the controls of a node with an auto generated ID. In a multi-client situation, the only way you can be sure what node -1 refers to is to put the messages in a bundle.

This message now supports array type tags ($[ and $]) in the control/value component of the OSC message. Arrayed control values are applied in the manner of n_setn (i.e., sequentially starting at the indexed or named control). See the Node Messaging helpfile.
*/

/*
/g_freeAll

Delete all nodes in a group.
N * int	group ID(s)

Frees all nodes in the group. A list of groups may be specified.
*/

/*
/g_new

Create a new group.
N *
int	new group ID
int	add action (0,1,2, 3 or 4 see below)
int	add target ID

Create a new group and add it to the tree of nodes. There are four ways to add the group to the tree as determined by the add action argument which is defined as follows (the same as for /s_new):

add actions:
    0	add the new group to the the head of the group specified by the add target ID.
    1	add the new group to the the tail of the group specified by the add target ID.
    2	add the new group just before the node specified by the add target ID.
    3	add the new group just after the node specified by the add target ID.
    4	the new node replaces the node specified by the add target ID. The target node is freed.

Multiple groups may be created in one command by adding arguments.
*/

func freeAll(wr io.Writer) error {
	return osc.Path("/g_freeAll").WriteTo(wr, groupID)
}

func createGroup(wr io.Writer) error {
	return osc.Path("/g_new").WriteTo(wr, groupID, int32(1), int32(0))
}

type listener struct {
	mx       sync.Mutex
	synthdef string
	channel  int8
	address  string
	rd       *mid.Reader
	wr       io.WriteCloser
	in       midi.In
}

type Listener interface {
	Listen(midi.In) error
	Close() error
}

func (p *listener) Listen(in midi.In) (err error) {
	if p.wr == nil {
		p.wr, err = osc.UDPWriter(p.address)
		if err != nil {
			return err
		}

		err = createGroup(p.wr)
		if err != nil {
			return err
		}
	}
	p.rd = mid.NewReader(mid.NoLogger())
	p.rd.Msg.Channel.NoteOn = func(_ *mid.Position, ch, key, vel uint8) {
		if p.channel < 0 || uint8(p.channel) == ch {
			nt := mkScNoteFromNoteOn(ch, key, vel)
			p.mx.Lock()
			nt.WriteOSC(p.synthdef, p.wr)
			p.mx.Unlock()
		}
	}
	p.rd.Msg.Channel.NoteOff = func(_ *mid.Position, ch, key, vel uint8) {
		if p.channel < 0 || uint8(p.channel) == ch {
			p.mx.Lock()
			osc.Path("/n_set").WriteTo(p.wr, scNoteID(ch, key), "gate", float32(0))
			p.mx.Unlock()
		}
	}
	p.in = in
	return mid.ConnectIn(in, p.rd)
}

func (p *listener) Close() error {
	p.mx.Lock()
	p.in.Close()
	freeAll(p.wr)
	p.wr.Close()
	p.mx.Unlock()
	return nil
}

func New(synthdef string, options ...Option) Listener {
	var p = &listener{}
	p.synthdef = synthdef
	p.channel = -1
	p.address = "127.0.0.1:57110"

	for _, opt := range options {
		opt(p)
	}

	return p
}

type Option func(*listener)

func SCAddress(addr string) Option {
	return func(p *listener) {
		p.address = addr
	}
}

func SCConnection(wr io.WriteCloser) Option {
	return func(p *listener) {
		p.wr = wr
	}
}

func Channel(ch uint8) Option {
	return func(p *listener) {
		p.channel = int8(ch)
	}
}
