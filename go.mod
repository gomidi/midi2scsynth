module gitlab.com/gomidi/midi2scsynth

go 1.14

require (
	gitlab.com/gomidi/midi v1.15.3
	gitlab.com/gomidi/rtmididrv v0.9.3
	gitlab.com/goosc/osc v0.2.0
	gitlab.com/metakeule/config v1.13.0
)
