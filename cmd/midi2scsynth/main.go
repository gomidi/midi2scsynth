package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi2scsynth"
	"gitlab.com/gomidi/rtmididrv"
	"gitlab.com/metakeule/config"
)

var (
	cfg         = config.MustNew("midi2scsynth", "0.2.1", "trigger scsynth via MIDI notes")
	argInPort   = cfg.NewInt32("in", "MIDI in port", config.Required, config.Shortflag('i'))
	argSynthDef = cfg.NewString("synth", "name of the synthdef", config.Default("default"), config.Shortflag('s'))
	argChannel  = cfg.NewInt32("channel", "MIDI channel to listen to (0 = all)", config.Default(int32(0)), config.Shortflag('c'))
	argAddress  = cfg.NewString("addr", "address where the scsynth is listening to", config.Default("127.0.0.1:57110"), config.Shortflag('a'))
	cmdList     = cfg.MustCommand("list", "list all MIDI in ports").Relax("in").Relax("synth")
)

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v", err)
		os.Exit(1)
	}
	os.Exit(0)
}

var sigchan = make(chan os.Signal, 10)

func run() error {
	err := cfg.Run()
	if err != nil {
		fmt.Fprintln(os.Stdout, cfg.Usage())
		return nil
	}

	drv, err := rtmididrv.New()
	if err != nil {
		return err
	}

	if cfg.ActiveCommand() == cmdList {
		ins, err := drv.Ins()
		if err != nil {
			return err
		}

		fmt.Fprintln(os.Stdout, "## MIDI in ports ##")

		for _, in := range ins {
			fmt.Fprintf(os.Stdout, "[%v] %s\n", in.Number(), in.String())
		}

		return drv.Close()
	}

	var opts = []midi2scsynth.Option{midi2scsynth.SCAddress(argAddress.Get())}

	if argChannel.Get() > 0 {
		opts = append(opts, midi2scsynth.Channel(uint8(argChannel.Get()-1)))
	}

	m := midi2scsynth.New(argSynthDef.Get(), opts...)
	in, err := midi.OpenIn(drv, int(argInPort.Get()), "")
	if err != nil {
		return err
	}

	err = m.Listen(in)
	if err != nil {
		return err
	}

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted...")

	return m.Close()
}
