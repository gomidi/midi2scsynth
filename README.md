# midi2scsynth
Converts MIDI data to OSC messages for the supercollider synth and sends them on the fly

## Status

usable, early stages. Only MIDI on/off messages are currently supported.

## Documentation

see http://godoc.org/github.com/gomidi/midi2scsynth

## Installation

```
go get -u -d github.com/gomidi/midi2scsynth/cmd/midi2scsynth
```

## Usage

use 

```
midi2scsynth list
```

to get a list of available MIDI in ports.

and

```
midi2scsynth -i=3 -s='theremin'
```

to use the MIDI data of port 3 to trigger synths of the synthdef "theremin".
For this to work, the scsynth must have been started on localhost and listening
on port 57110 (default), other addresses can be set via -a flag.
Also this example expects that the scsynth has been given a synthdef for 'theremin', e.g.

```
SynthDef(\theremin, {
	arg gate=1, freq, amp;
	var osc, env;
	env = EnvGen.ar( envelope:Env.asr(releaseTime:0.05), gate: gate, doneAction: 2 );
	osc = SinOsc.ar( freq );
	Out.ar(0, osc*env*amp*0.2);
}).add;
```

for further options:

```
midi2scsynth help
```

## Usage as a library

example code 
```go 
package main

import (
	"fmt"
	"os"
	"os/signal"

	"github.com/gomidi/connect"
	"github.com/gomidi/midi2scsynth"
	"github.com/gomidi/rtmididrv"
)

var sigchan = make(chan os.Signal, 10)

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v", err)
	}
}

func run() error {
	m := midi2scsynth.New("theremin")
	drv, err := rtmididrv.New()
	if err != nil {
		return err
	}
	in, err := connect.OpenIn(drv, 10, "")
	if err != nil {
		return err
	}

	err = m.Listen(in)
	if err != nil {
		return err
	}

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted...")

	return m.Close()
}

```